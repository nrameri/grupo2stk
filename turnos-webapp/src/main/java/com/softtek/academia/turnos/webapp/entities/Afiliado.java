package com.softtek.academia.turnos.webapp.entities;

import javax.persistence.Entity;
import java.util.Date;
import javax.persistence.*;

@Entity
@Table(name="Afiliados")
public class Afiliado {
	@Column(name="Nombre")
	private String nombre;
	@Column(name="Apellido")
	private String apellido;
	@Id
	@Column(name="Numero_afiliados")
	private int nro_afiliado;
	@ManyToOne(optional = false)
	@JoinColumn(name="Iddoc_tipo")
	private DocTipo doc_tipo;
	@Column(name="Doc_numero")
	private int doc_numero;
	@Column(name="Direccion")
	private String direccion;
	@Column(name="Telefono")
	private int telefono;
	@Column(name="Email")
	private String email;
	@Column(name="Nacimiento")
	private Date nacimento;
	@Column(name="Est_civil")
	private String est_civil;
	@Column(name="Hijos")
	private int hijos;
	@ManyToOne(optional = false)
	@JoinColumn(name="Idplan")
	private Plan plan;
	
	public Afiliado() {}

	public Afiliado(String nombre, String apellido, int nro_afiliado, DocTipo doc_tipo, int doc_numero,
			String direccion, int telefono, String email, Date nacimento, String est_civil, int hijos, Plan plan) {
		this.nombre = nombre;
		this.apellido = apellido;
		this.nro_afiliado = nro_afiliado;
		this.doc_tipo = doc_tipo;
		this.doc_numero = doc_numero;
		this.direccion = direccion;
		this.telefono = telefono;
		this.email = email;
		this.nacimento = nacimento;
		this.est_civil = est_civil;
		this.hijos = hijos;
		this.plan = plan;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public int getNro_afiliado() {
		return nro_afiliado;
	}

	public void setNro_afiliado(int nro_afiliado) {
		this.nro_afiliado = nro_afiliado;
	}

	public DocTipo getDoc_tipo() {
		return doc_tipo;
	}

	public void setDoc_tipo(DocTipo doc_tipo) {
		this.doc_tipo = doc_tipo;
	}

	public int getDoc_numero() {
		return doc_numero;
	}

	public void setDoc_numero(int doc_numero) {
		this.doc_numero = doc_numero;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public int getTelefono() {
		return telefono;
	}

	public void setTelefono(int telefono) {
		this.telefono = telefono;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getNacimento() {
		return nacimento;
	}

	public void setNacimento(Date nacimento) {
		this.nacimento = nacimento;
	}

	public String getEst_civil() {
		return est_civil;
	}

	public void setEst_civil(String est_civil) {
		this.est_civil = est_civil;
	}

	public int getHijos() {
		return hijos;
	}

	public void setHijos(int hijos) {
		this.hijos = hijos;
	}

	
	public Plan getPlan() {
		return plan;
	}

	public void setPlan(Plan plan) {
		this.plan = plan;
	}

	@Override
	public String toString() {
		return "Afiliado [nombre=" + nombre + ", apellido=" + apellido + ", nro_afiliado=" + nro_afiliado
				+ ", doc_tipo=" + doc_tipo + ", doc_numero=" + doc_numero + ", direccion=" + direccion + ", telefono="
				+ telefono + ", email=" + email + ", nacimento=" + nacimento + ", est_civil=" + est_civil + ", hijos="
				+ hijos + ", plan=" + plan + "]";
	}
	
	
}