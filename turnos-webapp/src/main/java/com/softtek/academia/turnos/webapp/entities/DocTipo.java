package com.softtek.academia.turnos.webapp.entities;

import javax.persistence.*;


@Entity
@Table(name="Doc_tipo")
public class DocTipo {
	@Id
	@Column(name="Iddoc_tipo")
    private int id;
	@Column(name="Nombre")
	private String nombre;
	
	public DocTipo() {}

	public DocTipo(int id, String nombre) {
		this.id = id;
		this.nombre = nombre;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public String toString() {
		return "DocTipo [id=" + id + ", nombre=" + nombre + "]";
	}
	
}
