package com.softtek.academia.turnos.webapp.action;

import java.util.List;

import com.opensymphony.xwork2.ActionSupport;
import com.softtek.academia.turnos.webapp.bo.PrestadorBo;
import com.softtek.academia.turnos.webapp.entities.Prestador;

public class PrestadoresActions extends ActionSupport{

	private static final long serialVersionUID = 5169089558417962070L;
	private List<Prestador> listaPrestadores;
	private PrestadorBo prestadorBo;
	
    public String verPrestadores() {
		this.listaPrestadores = prestadorBo.findListaPrestador();
		System.out.println(prestadorBo.findListaPrestador());
		return SUCCESS;
	}
	
	public String verFormulario() {
		
		return SUCCESS;
	}
	
	public String crearPrestadores() {
		
		return SUCCESS;
	}
	
	public String editarPrestadores() {
		
		return SUCCESS;
	}

	public String eliminarPrestadores() {
		
		return SUCCESS;
	}

	public void setPrestadorBo(PrestadorBo prestadorBo) {
		this.prestadorBo = prestadorBo;
	}

	public List<Prestador> getListaPrestadores() {
		return listaPrestadores;
	}

	public void setListaPrestadores(List<Prestador> listaPrestadores) {
		this.listaPrestadores = listaPrestadores;
	}
	

}