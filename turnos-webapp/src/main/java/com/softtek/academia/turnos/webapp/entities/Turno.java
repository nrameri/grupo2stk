package com.softtek.academia.turnos.webapp.entities;

import java.util.Date;

import javax.persistence.*;

@Entity
@Table(name="Turnos")
public class Turno {
	@Id
	@Column(name="Nro_turno")
	private int nro_turno;
	@ManyToOne(optional = false)
	@JoinColumn(name="Nro_afiliado")
	private Afiliado afiliado;
	@ManyToOne(optional = false)
	@JoinColumn(name="Profecional_matricula")
	private Prestador prestador;
	@Column(name="Fecha_hora_llegada")
	private Date fecha_hora_llegada;
	@Column(name="Fecha_hora_atencion")
	private Date fecha_hora_atencion;
	@Column(name="Importe")
	private double importe;
	@Column(name="Observaciones")
	private String observaciones;

	public Turno() {}

	public Turno(int nro_turno, Afiliado afiliado, Prestador prestador, Date fecha_hora_llegada,
			Date fecha_hora_atencion, double importe, String observaciones) {
		this.nro_turno = nro_turno;
		this.afiliado = afiliado;
		this.prestador = prestador;
		this.fecha_hora_llegada = fecha_hora_llegada;
		this.fecha_hora_atencion = fecha_hora_atencion;
		this.importe = importe;
		this.observaciones = observaciones;
	}

	public int getNro_turno() {
		return nro_turno;
	}

	public void setNro_turno(int nro_turno) {
		this.nro_turno = nro_turno;
	}

	public Afiliado getAfiliado() {
		return afiliado;
	}

	public void setAfiliado(Afiliado afiliado) {
		this.afiliado = afiliado;
	}

	public Prestador getPrestador() {
		return prestador;
	}

	public void setPrestador(Prestador prestador) {
		this.prestador = prestador;
	}

	public Date getFecha_hora_llegada() {
		return fecha_hora_llegada;
	}

	public void setFecha_hora_llegada(Date fecha_hora_llegada) {
		this.fecha_hora_llegada = fecha_hora_llegada;
	}

	public Date getFecha_hora_atencion() {
		return fecha_hora_atencion;
	}

	public void setFecha_hora_atencion(Date fecha_hora_atencion) {
		this.fecha_hora_atencion = fecha_hora_atencion;
	}

	public double getImporte() {
		return importe;
	}

	public void setImporte(double importe) {
		this.importe = importe;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	@Override
	public String toString() {
		return "Turno [nro_turno=" + nro_turno + ", afiliado=" + afiliado + ", prestador=" + prestador
				+ ", fecha_hora_llegada=" + fecha_hora_llegada + ", fecha_hora_atencion=" + fecha_hora_atencion
				+ ", importe=" + importe + ", observaciones=" + observaciones + "]";
	}
	
}
