package com.softtek.academia.turnos.webapp.dao;

import java.util.List;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;

import com.softtek.academia.turnos.webapp.entities.Turno;

public class TurnoDAO extends HibernateDaoSupport{
	public List<Turno> turnosTodos(){
		return (List<Turno>) getHibernateTemplate().find("from Turno");
	}
}
