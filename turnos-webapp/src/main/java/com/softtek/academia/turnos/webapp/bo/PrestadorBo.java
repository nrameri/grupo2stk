package com.softtek.academia.turnos.webapp.bo;

import java.util.List;

import com.softtek.academia.turnos.webapp.entities.Prestador;

public interface PrestadorBo {
	public List<Prestador> findListaPrestador();
}
