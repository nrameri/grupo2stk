package com.softtek.academia.turnos.webapp.entities;

import javax.persistence.*;

@Entity
@Table(name="Prestadores")
public class Prestador {
	@Id
	@Column(name="Profecional_matricula")
	private int id;
	@ManyToOne(optional = false)
	@JoinColumn(name="Nro_practica")
	private Practica practica;
	@Column(name="Nombre")
	private String nombre;
	@Column(name="Apellido")
	private String apellido;
	@Column(name="Direccion")
	private String direccion;
	@Column(name="Localidad")
	private String localidad;
	@Column(name="Telefono")
	private int telefono;
	
	public Prestador() {}

	public Prestador(int id, Practica practica, String nombre, String apellido, String direccion, String localidad,
			int telefono) {
		this.id = id;
		this.practica = practica;
		this.nombre = nombre;
		this.apellido = apellido;
		this.direccion = direccion;
		this.localidad = localidad;
		this.telefono = telefono;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Practica getPractica() {
		return practica;
	}

	public void setPractica(Practica practica) {
		this.practica = practica;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public int getTelefono() {
		return telefono;
	}

	public void setTelefono(int telefono) {
		this.telefono = telefono;
	}

	@Override
	public String toString() {
		return "Prestador [id=" + id + ", practica=" + practica + ", nombre=" + nombre + ", apellido=" + apellido
				+ ", direccion=" + direccion + ", localidad=" + localidad + ", telefono=" + telefono + "]";
	}
	
	
}
