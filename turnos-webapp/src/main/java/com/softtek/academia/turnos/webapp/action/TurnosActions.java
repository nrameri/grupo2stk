package com.softtek.academia.turnos.webapp.action;

import com.opensymphony.xwork2.ActionSupport;

public class TurnosActions extends ActionSupport{

	private static final long serialVersionUID = 8744722435417695199L;
	
	public String verTurnos() {
		
		return SUCCESS;
	}
	
	public String crearTurno() {
		
		return SUCCESS;
	}
	
	public String editarTurno() {
		
		return SUCCESS;
	}

	public String eliminarTurno() {
		
		return SUCCESS;
	}
}
