package com.softtek.academia.turnos.webapp.entities;

import java.util.List;

import javax.persistence.*;

@Entity
@Table(name="Planes")
public class Plan {
	@Id
	@Column(name="IdPlan")
    private int idPlan;
	@Column(name="Nombre")
	private String nombre;
//	@OneToMany(mappedBy = "IdPlan")
//	private List<Afiliado> afiliados;
	
	public Plan() {	}

	public Plan(int idPlan, String nombre) {
		this.idPlan = idPlan;
		this.nombre = nombre;
	}

	public int getIdPlan() {
		return idPlan;
	}

	public void setIdPlan(int idPlan) {
		this.idPlan = idPlan;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public String toString() {
		return "Plan [idPlan=" + idPlan + ", nombre=" + nombre + "]";
	}

}
