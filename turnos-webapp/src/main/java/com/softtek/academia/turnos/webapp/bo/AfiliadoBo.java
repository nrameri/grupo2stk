package com.softtek.academia.turnos.webapp.bo;

import java.util.List;

import com.softtek.academia.turnos.webapp.entities.Afiliado;

public interface AfiliadoBo {
	public List<Afiliado> findListaPacientes();
}
