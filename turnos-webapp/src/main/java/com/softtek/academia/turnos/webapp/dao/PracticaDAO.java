package com.softtek.academia.turnos.webapp.dao;

import java.util.List;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import com.softtek.academia.turnos.webapp.entities.Practica;

public class PracticaDAO extends HibernateDaoSupport{
	public List<Practica> turnosTodos(){
		return (List<Practica>) getHibernateTemplate().find("from Practica");
	}
}
