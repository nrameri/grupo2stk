package com.softtek.academia.turnos.webapp.action;

import java.util.List;

import com.opensymphony.xwork2.ActionSupport;
import com.softtek.academia.turnos.webapp.bo.AfiliadoBo;
import com.softtek.academia.turnos.webapp.entities.Afiliado;
import com.softtek.academia.turnos.webapp.entities.DocTipo;

public class PacientesActions extends ActionSupport{

	private static final long serialVersionUID = -4766968325766715425L;
	private List<DocTipo> docTipo;
	private List<Afiliado> listaPacientes;
	private AfiliadoBo afiliadoBo;
	
	
	public void setAfiliadoBo(AfiliadoBo afiliadoBo) {
		this.afiliadoBo = afiliadoBo;
	}

	public String verPacientes() {
		this.listaPacientes = afiliadoBo.findListaPacientes();
		System.out.println(afiliadoBo.findListaPacientes()+"del pacientes action");
		return SUCCESS;
	}
	
	public String verFormulario() {
		
		return SUCCESS;
	}
	
	public String crearPaciente() {
		
		return SUCCESS;
	}
	
	public String editarPaciente() {
		
		return SUCCESS;
	}

	public String eliminarPaciente() {
		
		return SUCCESS;
	}

	public List<Afiliado> getListaPacientes() {
		return listaPacientes;
	}

	public void setListaPacientes(List<Afiliado> listaPacientes) {
		this.listaPacientes = listaPacientes;
	}

	public List<DocTipo> getDocTipo() {
		return docTipo;
	}

	public void setDocTipo(List<DocTipo> docTipo) {
		this.docTipo = docTipo;
	}

	
}
