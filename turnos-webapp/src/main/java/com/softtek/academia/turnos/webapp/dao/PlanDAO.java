package com.softtek.academia.turnos.webapp.dao;

import java.util.List;

import org.springframework.orm.hibernate4.support.HibernateDaoSupport;

import com.softtek.academia.turnos.webapp.entities.Plan;

public class PlanDAO extends HibernateDaoSupport {
    public List<Plan> find() {
        return (List<Plan>) getHibernateTemplate().find("from Plan");
    }
}
