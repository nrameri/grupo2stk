package com.softtek.academia.turnos.webapp.bo.impl;

import java.util.List;

import com.softtek.academia.turnos.webapp.bo.PrestadorBo;
import com.softtek.academia.turnos.webapp.dao.PrestadorDAO;
import com.softtek.academia.turnos.webapp.entities.Prestador;

public class PrestadorBoImpl implements PrestadorBo{
	private PrestadorDAO prestadorDAO;
	
	@Override
	public List<Prestador> findListaPrestador() {
		return prestadorDAO.turnosTodos();
	}
	
	public PrestadorDAO getPrestadorDAO() {
		return prestadorDAO;
	}
	
	public void setPrestadorDAO(PrestadorDAO prestadorDAO) {
		this.prestadorDAO = prestadorDAO;
	}

}
