package com.softtek.academia.turnos.webapp.entities;

import javax.persistence.*;

@Entity
@Table(name="Practicas")
public class Practica {
	@Id
	@Column(name="Nro_practica")
	private int nro_practica;
	@Column(name="Nombre")
	private String nombre;
	@Column(name="Observaciones")
	private String observacion;
	
	public Practica() {	}

	public Practica(int nro_practica, String nombre, String observacion) {
		this.nro_practica = nro_practica;
		this.nombre = nombre;
		this.observacion = observacion;
	}

	public int getNro_practica() {
		return nro_practica;
	}

	public void setNro_practica(int nro_practica) {
		this.nro_practica = nro_practica;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	@Override
	public String toString() {
		return "Practica [nro_practica=" + nro_practica + ", nombre=" + nombre + ", observacion=" + observacion + "]";
	}
	
	
}
