package com.softtek.academia.turnos.webapp.dao;

import java.util.List;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import com.softtek.academia.turnos.webapp.entities.DocTipo;

public class DocTipoDAO extends HibernateDaoSupport{
	public List<DocTipo> find() {
        return (List<DocTipo>) getHibernateTemplate().find("from DocTipo");
    }
}
