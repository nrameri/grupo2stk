package com.softtek.academia.turnos.webapp.bo.impl;

import java.util.List;

import com.softtek.academia.turnos.webapp.bo.AfiliadoBo;
import com.softtek.academia.turnos.webapp.dao.AfiliadoDAO;
import com.softtek.academia.turnos.webapp.entities.Afiliado;

public class AfiliadoBoImpl implements AfiliadoBo{
	private AfiliadoDAO afiliadoDao;
	
	public AfiliadoDAO getAfiliadoDao() {
		return afiliadoDao;
	}

	public void setAfiliadoDao(AfiliadoDAO afiliadoDao) {
		this.afiliadoDao = afiliadoDao;
	}

	@Override
	public List<Afiliado> findListaPacientes() {
		return afiliadoDao.find();
	}
	
}
