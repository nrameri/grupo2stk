package com.softtek.academia.turnos.webapp.dao;

import java.util.List;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import com.softtek.academia.turnos.webapp.entities.Prestador;

public class PrestadorDAO extends HibernateDaoSupport{
	public List<Prestador> turnosTodos(){
		return (List<Prestador>) getHibernateTemplate().find("from Prestador");
	}

}
