package com.softtek.academia.turnos.webapp.dao;

import java.util.List;

import org.springframework.orm.hibernate4.support.HibernateDaoSupport;

import com.softtek.academia.turnos.webapp.entities.Afiliado;

public class AfiliadoDAO extends HibernateDaoSupport {
	public List<Afiliado> find() {
        return (List<Afiliado>) getHibernateTemplate().find("from Afiliado");
    }
}
