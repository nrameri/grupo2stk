package com.softtek.academia.turnos.webapp.action;

import org.springframework.beans.factory.annotation.Value;

import com.opensymphony.xwork2.ActionSupport;
import com.softtek.academia.turnos.webapp.bo.UserBo;
import com.softtek.academia.turnos.webapp.dao.AfiliadoDAO;
import com.softtek.academia.turnos.webapp.dao.PlanDAO;

public class LoginAction extends ActionSupport{
	private static final long serialVersionUID = -6017059794546455776L;
	private String email;
	@Value("${pass}")
	private String pass;
	private UserBo userBo;
	@Value("${mensaje}")
	private String mensaje;
	private PlanDAO planDao;
	private AfiliadoDAO afiliadoDao;
	
	public PlanDAO getPlanDao() {
		return planDao;
	}

	public void setPlanDao(PlanDAO planDao) {
		this.planDao = planDao;
		System.out.println(planDao.find());
	}

	public String ingresar() {
		return SUCCESS;
	}
	
	public String bienvenido() {
//		setMensaje(userBo.printUser(email));
	
		return SUCCESS;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public UserBo getUserBo() {
		return userBo;
	}

	public void setUserBo(UserBo userBo) {
		this.userBo = userBo;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public AfiliadoDAO getAfiliadoDao() {
		return afiliadoDao;
	}

	public void setAfiliadoDao(AfiliadoDAO afiliadoDao) {
		this.afiliadoDao = afiliadoDao;
		System.out.println(afiliadoDao.find());
	}
	
	
}
