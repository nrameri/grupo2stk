<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="s" uri="/struts-tags" %>
    
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="utf-8">
<title>Alta Paciente</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/global.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/forms.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
    rel="stylesheet">
</head>
  <body>

    <div id="header">
      <div class="logo">
        <a href="#">Turnos-<span>Webapp</span></a>
      </div>
      <div class="logout">
        <div class="ic-salida"><i class="material-icons md-light">exit_to_app</i></div>
        <a href="#">Logout</a>
      </div>
    </div>
    <div id="container">
      <div class="sidebar">
        <ul id="nav">
          <li>
            <a href="<s:url action="verPacientes"/>">
              <i class="material-icons">list</i><p>Listado Pacientes</p>
            </a>
          </li>
          <li>
            <a class="selected" href="<s:url action="verFormPaciente"/>">
              <i class="material-icons">face</i><p>Alta Pacientes</p>
            </a>
          </li>
          <li>
            <a href="verTurnos">
              <i class="material-icons">list</i><p>Listado Turnos</p>
            </a>
          </li>
          <li>
            <a href="abmTurnos">
              <i class="material-icons">note_add</i><p>Alta Turnos</p>
            </a>
          </li>
          <li>
            <a href="<s:url action="verPrestadores"/>">
              <i class="material-icons">list</i><p>Listado Prestadores</p>
            </a>
          </li>
          <li>
            <a href="<s:url action="verFormPrestadores"/>">
              <i class="material-icons">person_pin</i><p>Alta Prestador</p>
            </a>
          </li>


        </ul>
      </div>

      <div class="content">
       
      <h1>Alta Pacientes</h1>
      <form>

        <br><br>
        <label for="Nombre:">Nombre:</label>
        <input class="imp3" type="text" name="firstname" placeholder="Nombre">
        
        <label for="Apellido">Apellido</label>
        <input  class="imp3" type="text" name="lastname" placeholder="Apellido">
        <br><br>
        <label for="Nro  y tipo de dni">Nro y tipo de dni</label>
        <input   class="imp3" type="text" name="firstname" placeholder="DNI">

        <select  class="imp3" name="TIPO">
          <option value="DNI">DNI</option>
          <option value="LC">LC</option>
          <option value="LE">LE</option>
          <option value="PASAPORTE">PASAPORTE</option>
        </select>

         <br><br>
        <label for="Direccion">Direccion</label>
        <input class="imp2" type="text" name="firstname" placeholder="Direccion">
        <br><br>
        <label for="EstadoCivil">EstadoCivil</label>
        <select class="imp3"name="EstadoCivil">
          <option value="Soltero/a">Soltero/a</option>
          <option value="Casado/a">Casado/a</option>
          <option value="Viudo/a">Viudo/a</option>
        </select>
   
        <label for="Cantidad de hijos ">Cantidad de hijos </label>
        <input class="imp3" type="text" name="lastname" placeholder="Hijos">
       <br><br>
       <label for="Telefonos">Telefonos</label>
       <input class="imp3"type="text" name="lastname" placeholder="Telefonos">

       <label for="Email">Email</label>
       <input class="imp3"type="text" name="lastname" placeholder="email">
       <br><br>
       <label for="Fecha de nacimiento">Fecha de nacimiento</label>
       <input class="imp3"type="date"/>  
       <label  for="Plan">Plan</label>
      
      <select class="imp3" name="plan">
      <option value="210">210</option>
      <option value="310">310</option>
      <option value="410">410</option>
      <option value="450">450</option>
       <option value="510">510</option>
      </select>
      <br><br>
      <input  class = "boton"  type="submit" value="Registrar">
  
      </form>

      </div>
    </div>
  </body>
</html>