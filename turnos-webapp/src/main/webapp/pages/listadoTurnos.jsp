<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<title>Alta Turno</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/global.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/forms.css">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
</head>

    <div id="header">
      <div class="logo">
        <a href="#">Turnos-<span>Webapp</span></a>
      </div>
      <div class="logout">
        <div class="ic-salida"><i class="material-icons md-light">exit_to_app</i></div>
        <a href="#">Logout</a>
      </div>
    </div>

    <div id="container">
      <div class="sidebar">
        <ul id="nav">
          <li>
            <a href="paciente">
              <i class="material-icons">list</i><p>Listado Pacientes</p>
            </a>
          </li>
          <li>
            <a href="verFormPaciente">
              <i class="material-icons">face</i><p>Alta Pacientes</p>
            </a>
          </li>
          <li>
            <a href="verTurnos">
              <i class="material-icons">list</i><p>Listado Turnos</p>
            </a>
          </li>
          <li>
            <a href="abmTurnos">
              <i class="material-icons">note_add</i><p>Alta Turnos</p>
            </a>
          </li>
          <li>
            <a href="prestadores">
              <i class="material-icons">list</i><p>Listado Prestadores</p>
            </a>
          </li>
          <li>
            <a href="verFormPrestadores">
              <i class="material-icons">person_pin</i><p>Alta Prestador</p>
            </a>
          </li>
        </ul>
      </div>

      <div class="content">
        <h1>Listado de Turnos</h1>

        <div class="functions">
          <i class="material-icons">add</i>
          <i class="material-icons">remove</i>
          <i class="material-icons">mode_edit</i>
        </div>

        <div class="sub-content">
          <table cellspacing="0">
            <tr>
                <th><a href="#"><i class="material-icons">expand_more</i></a> Nro. Turno</th>
                <th><a href="#"><i class="material-icons">expand_more</i></a> Nombre</th>
                <th><a href="#"><i class="material-icons">expand_more</i></a> Apellido</th>
                <th><a href="#"><i class="material-icons">expand_more</i></a> Nro. Afiliado</th>
                <th><a href="#"><i class="material-icons">expand_more</i></a> Plan medico</th>
            </tr>
            <tr>
                <td>000001</td>
                <td>Claudio Paul</td>
                <td>Canigia</td>
                <td>65866542654</td>
                <td>510</td>
            </tr>
          </table>

          <ul class="pagination">
            <li><a href="#">�</a></li>
            <li><a href="#">1</a></li>
            <li><a class="active" href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">5</a></li>
            <li><a href="#">�</a></li>
          </ul>
        </div>
      </div>
    </div>

  </body>
</html>