<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
            <%@ taglib prefix="s" uri="/struts-tags" %>
    
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="utf-8">
<title>Alta Prestadores</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/global.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/forms.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
    rel="stylesheet">
</head>
<body>

 <div id="header">
      <div class="logo">
        <a href="#">Turnos-<span>Webapp</span></a>
      </div>
      <div class="logout">
        <div class="ic-salida"><i class="material-icons md-light">exit_to_app</i></div>
        <a href="#">Logout</a>
      </div>
    </div>

    <div id="container">
      <div class="sidebar">
        <ul id="nav">
          <li>
            <a  href="<s:url action="verPacientes"/>">
              <i class="material-icons">list</i><p>Listado Pacientes</p>
            </a>
          </li>
          <li>
            <a href="<s:url action="verFormPaciente"/>">
              <i class="material-icons">face</i><p>Alta Pacientes</p>
            </a>
          </li>
          <li>
            <a href="verTurnos">
              <i class="material-icons">list</i><p>Listado Turnos</p>
            </a>
          </li>
          <li>
            <a href="abmTurnos">
              <i class="material-icons">note_add</i><p>Alta Turnos</p>
            </a>
          </li>
          <li>
            <a href="<s:url action="verPrestadores"/>">
              <i class="material-icons">list</i><p>Listado Prestadores</p>
            </a>
          </li>
          <li>
            <a class="selected" href="<s:url action="verFormPrestadores"/>">
              <i class="material-icons">person_pin</i><p>Alta Prestador</p>
            </a>
          </li>
        </ul>
      </div>

      <div class="content">
    
    

      <h1>Alta Prestadores</h1>
          <form>

			  <br><br>
			  <label for="Nombre:">Nombre:</label>
			  <input class="imp3" type="text" name="firstname" placeholder="Nombre">
			  
			  <label for="Apellido">Apellido</label>
			  <input  class="imp3" type="text" name="lastname" placeholder="Apellido">
			  <br><br>
			    <label for="Matricula">Matricula</label>
			    <input   class="imp3" type="text" name="firstname" placeholder="Numero de matricula">
			
			   <br><br>
			    <label for="Direccion">Direccion</label>
			    <input class="imp2" type="text" name="firstname" placeholder="Direccion">
			    <br><br>
			
			 
			  <label for="Localidad ">Localidad </label>
			  <input class="imp3" type="text" name="lastname" placeholder="Localidad">
			     <br><br>
			  <label for="Telefonos">Telefonos</label>
			  <input class="imp3"type="text" name="lastname" placeholder="Telefonos">
 

		  	<label  for="Numero de practica">Numero de practica</label>
		    
		    <select class="imp3" name="plan">
		    <option value="10">10</option>
		    <option value="20">20</option>
		    <option value="30">30</option>
		    <option value="40">40</option>
		     <option value="50">50</option>
		    </select>
		
		     <br><br>
		    <input  class = "boton"  type="submit" value="Registrar">
  
       </form>

  	</div>
   </div>



</body>
</html>