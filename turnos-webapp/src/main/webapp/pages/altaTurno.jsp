<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<title>Alta Turno</title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/global.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/forms.css">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
</head>
<body>

	<div id="header">
		<div class="logo">
			<a href="#">Listado Turnos-<span>Webapp</span></a>
		</div>
		<div class="logout">
			<div class="ic-salida">
				<i class="material-icons md-light">exit_to_app</i>
			</div>
			<a href="#">Logout</a>
		</div>
	</div>

	<div id="container">
		<div class="sidebar">
			<ul id="nav">
				<li><a href="paciente"> <i
						class="material-icons">list</i>
						<p>Listado Pacientes</p>
				</a></li>
				<li><a href="verFormPaciente"> <i class="material-icons">face</i>
						<p>Alta Pacientes</p>
				</a></li>
				<li><a href="verTurnos"> <i class="material-icons">list</i>
					<p>Listado Turnos</p>
				</a></li>
				<li><a href="abmTurnos"> <i class="material-icons">note_add</i>
					<p>Alta Turnos</p>
				</a></li>
				<li><a href="verPrestadores"> <i
						class="material-icons">list</i>
						<p>Listado Prestador</p>
				</a></li>
				<li><a href="verFormPrestadores"> <i
						class="material-icons">person_pin</i>
						<p>Alta Prestador</p>
				</a></li>
			</ul>
		</div>

		<div class="content">
			<h1>Alta de turnos</h1>



			<div class="sub-content">


				<form>


					<label for="Practica">Practica</label> <select class="imp3"
						name="Practica">
						<option value="Traumato">Traumatologia</option>
						<option value="Pediatri">Pediatria</option>
						<option value="Oftalmol">Oftalmologia</option>
						<option value="Clinico">Clinico</option>

					</select> <br> <br> <label for="NTurno ">Nro Turno</label> <input
						class="imp3" type="text" name="nTurno"> <label
						for="ProfecionalM ">Matricula</label> <input class="imp3"
						type="text" name="nTurno"> <br> <br> <label
						for="Plan">Plan</label> <select class="imp3" name="Plan">
						<option value="210">P210</option>
						<option value="310">P310</option>
						<option value="410">P410</option>
						<option value="450">P450</option>
						<option value="510">P510</option>

					</select> <label for="Observaciones">Observaciones</label> <input
						class="imp3" type="text" name="Observaciones"
						placeholder="Es Humano"> <br> <br> <label
						for="Fecha del turno">Fecha</label> <input class="imp3"
						type="date" /> <label for="Fecha">Turno</label> <select
						class="imp3" name="Turno">
						<option value="1">9:00</option>
						<option value="2">10:00</option>
						<option value="3">11:00</option>
						<option value="4">12:00</option>
						<option value="5">13:00</option>
						<option value="6">14:00</option>
						<option value="7">15:00</option>
						<option value="8">16:00</option>
					</select> <br> <br> <input class="boton" type="submit"
						value="Registrar">

				</form>
			</div>
		</div>
	</div>



</body>
</html>