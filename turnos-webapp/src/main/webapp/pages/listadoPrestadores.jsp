<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="utf-8">
<title>Listado Pacientes</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/global.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/lista.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
</head>
<body>
<div id="header">
      <div class="logo">
        <a href="#">Turnos-<span>Webapp</span></a>
      </div>
      <div class="logout">
        <div class="ic-salida"><i class="material-icons md-light">exit_to_app</i></div>
        <a href="#">Logout</a>
      </div>
    </div>

    <div id="container">
      <div class="sidebar">
        <ul id="nav">
          <li>
            <a  href="<s:url action="verPacientes"/>">
              <i class="material-icons">list</i><p>Listado Pacientes</p>
            </a>
          </li>
          <li>
            <a href="<s:url action="verFormPaciente"/>">
              <i class="material-icons">face</i><p>Alta Pacientes</p>
            </a>
          </li>
          <li>
            <a href="verTurnos">
              <i class="material-icons">list</i><p>Listado Turnos</p>
            </a>
          </li>
          <li>
            <a href="abmTurnos">
              <i class="material-icons">note_add</i><p>Alta Turnos</p>
            </a>
          </li>
          <li>
            <a  class="selected" href="<s:url action="verPrestadores"/>">
              <i class="material-icons">list</i><p>Listado Prestadores</p>
            </a>
          </li>
          <li>
            <a href="<s:url action="verFormPrestadores"/>">
              <i class="material-icons">person_pin</i><p>Alta Prestador</p>
            </a>
          </li>
        </ul>
      </div>

      <div class="content">
        <h1>Listado de Pacientes</h1>

        <div class="functions">
          <i class="material-icons">add</i>
          <i class="material-icons">remove</i>
          <i class="material-icons">mode_edit</i>
          <i class="material-icons">picture_as_pdf</i>
        </div>

        <div class="sub-content">
          <table cellspacing="0">
            <tr>
                <th><a href="#"><i class="material-icons">expand_more</i></a></th>
                <th><a href="#"><i class="material-icons">expand_more</i></a> Nombre</th>
                <th><a href="#"><i class="material-icons">expand_more</i></a> Apellido</th>
                <th><a href="#"><i class="material-icons">expand_more</i></a> Matricula</th>

            </tr>
            <s:iterator value="listaPrestadores">
            <tr>
                <td class="check"><input type="checkbox"></td>
                <td><s:property value="nombre" /></td>
                <td><s:property value="apellido" /></td>
                <td><s:property value="id" /></td>
            </tr>
            </s:iterator>
          </table>

          <ul class="pagination">
            <li><a href="#">«</a></li>
            <li><a href="#">1</a></li>
            <li><a class="active" href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">5</a></li>
            <li><a href="#">»</a></li>
          </ul>
        </div>
      </div>
    </div>
</body>
</html>