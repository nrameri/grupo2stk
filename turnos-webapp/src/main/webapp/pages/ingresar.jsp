<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Ingresar</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/login.css">
</head>
<div class="login">
	<h1>Login</h1>
    <form action="bienvenido" method="post">
    	<input type="text" name="email" placeholder="Coloque su Email" required="required" />
        <input type="password" name="pass" placeholder="Password"  />
        <button type="submit" class="btn btn-primary btn-block btn-large">Ingresar.</button>
    </form>
</div>

</body>
</html>